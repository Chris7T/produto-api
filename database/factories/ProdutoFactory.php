<?php

namespace Database\Factories;

use App\Models\Categoria;
use App\Models\Produto;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProdutoFactory extends Factory
{
    protected $model = Produto::class;

    public function definition()
    {
        return [
            'nome'         => $this->faker->realTextBetween(4, 25),
            'descricao'    => $this->faker->realTextBetween(4, 100),
            'valor'        => $this->faker->randomFloat(2, 0, 300),
            'categoria_id' => Categoria::factory()->create()->getKey(),
        ];
    }
}
