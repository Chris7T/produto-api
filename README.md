# Api de Produtos

## Resumo

Api para gerenciar produtos e categoriza-los.

## Instacão Local

Instale utilizando o composer.
```
composer install
```

## Utilização do Docker
O projeto faz uso do ``Laradock``, ele possui todas as ferramentas para seu ambiente funcionar perfeitamente.\
Assim você já terá o banco de dados e o backend já servido.

- O Banco de dados utilizado é o `postgresSQL`.

- Também é feito uso do `nginx` para servir o sistema. 

Para usa-lo basta entrar na pasta `laradock` e executar os seguintes comandos :

```
docker-compose up -d nginx postgres
```

```
docker exec -it product-api_workspace_1 /bin/sh
```

## Documentação

O Sistema utiliza o ``scribe`` para documentação. Para gera-la basta usar o comando.
```
php artisan scribe:generate
```
Depois ela poderá ser acessada por ``base_url/docs`` 

## Testes
O Laravel tem suporte para testes com ``PHPUnit``. Para rodar os testes basta usar o comando abaixo.

```
php artisan test
```
