<?php

namespace App\Http\Controllers;

use App\Http\Requests\Categoria\CriarCategoriaRequest as CriarRequest;
use App\Http\Requests\Categoria\AtualizarCategoriaRequest as AtualizarRequest;
use App\Http\Resources\CategoriaResource as Resource;
use App\Models\Categoria;
use Illuminate\Http\Resources\Json\JsonResource;
use Symfony\Component\HttpFoundation\JsonResponse;

class CategoriaController extends Controller
{
    /**
     * Listar dados de Categorias
     *
     * Retorna todos os registros
     * @group Categorias
     * @responseFile ApiRespostas/CategoriaController/Listar.json
     */
    public function index(): JsonResource
    {
        return Resource::collection((Categoria::paginate(10)));
    }

    /**
     * Criar nova Categoria
     *
     * Cria nova Categoria
     * @group Categorias
     * @responseFile 201 ApiRespostas/CategoriaController/Buscar.json
     * @responseFile 422 ApiRespostas/CategoriaController/ValidacaoCriar.json
     */
    public function store(CriarRequest $request): JsonResponse
    {
        $novo = Categoria::create($request->all());
        return (new Resource($novo))->response()->setStatusCode(201);
    }

    /**
     * Buscar Categoria
     *
     * Retorna os dados da Categoria
     * @group Categorias
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/CategoriaController/Buscar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Categoria]"}
     */
    public function show(Categoria $categoria): JsonResource
    {
        return new Resource($categoria);
    }

    /**
     * Atualizar Categoria
     *
     * Atualizar dados Categoria
     * @group Categorias
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/CategoriaController/Buscar.json
     * @responseFile 422 ApiRespostas/CategoriaController/ValidacaoAtualizar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Categoria]"}
     */
    public function update(AtualizarRequest $request, Categoria $categoria): JsonResource
    {
        $categoria->update($request->validated());
        return new Resource($categoria);
    }

    /**
     * Excluir Categoria
     *
     * Exclui a Categoria
     * @group Categorias
     * @urlParam id integer required O id do registro.
     * @response 409 {"message": "Categoria está vinculada a um produto existente"}
     * @response 404 {"message": "No query results for model [App\\Models\\Categoria]"}
     */
    public function destroy(Categoria $categoria): JsonResponse
    {
        $categoria->delete();
        return response()->json(['message' => 'Ok']);
    }
}
