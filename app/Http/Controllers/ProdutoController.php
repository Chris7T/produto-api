<?php

namespace App\Http\Controllers;

use App\Http\Requests\Produto\AtualizarProdutoRequest as AtualizarRequest;
use App\Http\Requests\Produto\CriarProdutoRequest as CriarRequest;
use App\Http\Requests\Produto\JurosProdutoRequest as JurosRequest;
use App\Http\Resources\ProdutoResource as Resource;
use RegrasNegocio\Produto as Regras;
use App\Models\Produto;
use Illuminate\Http\JsonResponse as HttpJsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProdutoController extends Controller
{
    /**
     * Listar dados de Produtos
     *
     * Retorna todos os registros
     * @group Produtos
     * @responseFile ApiRespostas/ProdutoController/Listar.json
     */
    public function index(): JsonResource
    {
        return Resource::collection((Produto::paginate(10)));
    }

    /**
     * Criar nova Produto
     *
     * Cria nova Produto
     * @group Produtos
     * @responseFile 201 ApiRespostas/ProdutoController/Buscar.json
     * @responseFile 422 ApiRespostas/ProdutoController/ValidacaoCriar.json
     */
    public function store(CriarRequest $request): HttpJsonResponse
    {
        $novo = Produto::create($request->all());
        return (new Resource($novo))->response()->setStatusCode(201);
    }

    /**
     * Buscar Produto
     *
     * Retorna os dados da Produto
     * @group Produtos
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/ProdutoController/Buscar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Produto]"}
     */
    public function show(Produto $produto): JsonResource
    {
        return new Resource($produto);
    }

    /**
     * Atualizar Produto
     *
     * Atualizar dados Produto
     * @group Produtos
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/ProdutoController/Buscar.json
     * @responseFile 422 ApiRespostas/ProdutoController/ValidacaoAtualizar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Produto]"}
     */
    public function update(AtualizarRequest $request, Produto $produto): JsonResource
    {
        $produto->update($request->validated());
        return new Resource($produto);
    }

    /**
     * Excluir Produto
     *
     * Exclui o Produto
     * @group Produtos
     * @urlParam id integer required O id do registro.
     * @response 200 {"message": "OK"}
     * @response 404 {"message": "No query results for model [App\\Models\\Produto]"}
     */
    public function destroy(Produto $produto): JsonResponse
    {
        $produto->delete();
        return response()->json(['message' => 'Ok']);
    }

    /**
     * Mensalidade
     *
     * Mensalidade do Produto
     * @group Produtos
     * @responseFile ApiRespostas/ProdutoController/Mensalidades.json
     * @responseFile 422 ApiRespostas/ProdutoController/ValidacaoMensalidade.json
     */
    public function mensalidades(JurosRequest $request, Regras $regras): JsonResponse
    {
        return $regras->cacularJuros($request);
    }
}
