<?php

namespace App\Http\Requests\Produto;

class CriarProdutoRequest extends ProdutoRequest
{
    public function rules(): array
    {
        return [
            'nome'         => ['required', 'string', 'max:100'],
            'descricao'    => ['required', 'string', 'max:100'],
            'valor'        => ['required', 'numeric', 'gte:0', 'regex:/^\d+(\.\d{1,2})?$/'],
            'categoria_id' => ['required', 'integer', 'exists:categorias,id']
        ];
    }
}
