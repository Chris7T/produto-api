<?php

namespace App\Http\Requests\Produto;

use Illuminate\Foundation\Http\FormRequest;

class ProdutoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function bodyParameters(): array
    {
        return [
            'nome' => [
                'description' => 'Nome do Produto.',
                'example'     => 'Microondas'
            ],
            'descricao' => [
                'description' => 'Descrição do Produto.',
                'example'     => 'Marca X'
            ],
            'valor' => [
                'description' => 'Valor do Produto.',
                'example'     => '499.90'
            ],
            'categoria_id' => [
                'description' => 'ID da Categoria do Produto.',
                'example'     => 'Eletrodomésticos'
            ]
        ];
    }
}
