<?php

namespace App\Http\Requests\Produto;

use App\Rules\tipoJuros;
use Illuminate\Foundation\Http\FormRequest;

class JurosProdutoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'produto_id'      => ['required', 'integer', 'exists:produtos,id'],
            'numero_parcelas' => ['required', 'integer', 'gte:0', 'max:12'],
            'tipo_juros'      => ['required', 'string', 'max:8', new tipoJuros],
        ];
    }

    public function bodyParameters(): array
    {
        return [
            'produto_id' => [
                'description' => 'Id do Produto.',
                'example'     => 1
            ],
            'numero_parcelas' => [
                'description' => 'Quantidade de Parcelas.',
                'example'     => 5
            ],
            'tipo_juros' => [
                'description' => 'Tipo de Juros.',
                'example'     => 'Composto'
            ]
        ];
    }
}
