<?php

namespace App\Http\Requests\Produto;

class AtualizarProdutoRequest extends ProdutoRequest
{
    public function rules(): array
    {
        return [
            'nome'         => ['filled', 'string', 'max:100'],
            'descricao'    => ['filled', 'string', 'max:100'],
            'valor'        => ['filled', 'numeric', 'gte:0', 'regex:/^\d+(\.\d{1,2})?$/'],
            'categoria_id' => ['filled', 'integer', 'exists:categorias,id']
        ];
    }
}
