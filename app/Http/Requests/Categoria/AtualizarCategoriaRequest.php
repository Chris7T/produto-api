<?php

namespace App\Http\Requests\Categoria;

class AtualizarCategoriaRequest  extends CategoriaRequest
{
    public function rules(): array
    {
        return [
            'nome' => ['filled', 'string', 'max:100'],
            'taxa' => ['filled', 'gte:0', 'regex:/^\d+(\.\d{1,2})?$/'],
        ];
    }
}
