<?php

namespace App\Http\Requests\Categoria;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function bodyParameters(): array
    {
        return [
            'nome' => [
                'description' => 'Nome da Categoria.',
                'example'     => 'Eletrodomésticos.'
            ],
            'taxa' => [
                'description' => 'Taxa da Categoria.',
                'example'     => 1.2
            ]
        ];
    }
}
