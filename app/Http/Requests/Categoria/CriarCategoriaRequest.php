<?php

namespace App\Http\Requests\Categoria;

class CriarCategoriaRequest extends CategoriaRequest
{
    public function rules(): array
    {
        return [
            'nome' => ['required', 'string', 'max:100'],
            'taxa' => ['required', 'gte:0', 'regex:/^\d+(\.\d{1,2})?$/'],
        ];
    }
}
