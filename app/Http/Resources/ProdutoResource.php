<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProdutoResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'             => $this->getKey(),
            'nome'           => $this->nome,
            'descricao'      => $this->descricao,
            'valor'          => $this->valor,
            'categoria_id'   => $this->categoria->id,
            'categoria_nome' => $this->categoria->nome
        ];
    }
}
