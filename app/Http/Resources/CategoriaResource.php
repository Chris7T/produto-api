<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoriaResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'   => $this->getKey(),
            'nome' => $this->nome,
            'taxa' => $this->taxa,
        ];
    }
}
