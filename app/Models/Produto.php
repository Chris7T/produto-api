<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    use HasFactory;

    protected $table      = 'produtos';
    protected $primaryKey = 'id';
    protected $fillable   = [
        'nome',
        'descricao',
        'valor',
        'nome',
        'categoria_id'
    ];

    protected $casts = [
        'valor'        => 'float',
        'categoria_id' => 'int',
    ];

    public function categoria()
    {
        return $this->belongsTo(Categoria::class);
    }
}
