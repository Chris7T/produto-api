<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use HasFactory;

    protected $table      = 'categorias';
    protected $primaryKey = 'id';
    protected $fillable   = [
        'nome',
        'taxa'
    ];

    protected $casts = [
        'taxa' => 'float',
    ];

    public function produto()
    {
        return $this->hasOne(Produto::class);
    }

    protected static function booted()
    {
        static::deleting(function ($categoria) {
            abort_if($categoria->produto()->exists(), 409, 'Categoria está vinculada a um produto existente.');
        });
    }
}
