<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class tipoJuros implements Rule
{

    public function __construct()
    {
        //
    }

    public function passes($atributo, $valor)
    {
        if ($valor == 'Simples' || $valor == 'Composto') {
            return true;
        }
    }

    public function message()
    {
        return 'Tipo de Juros não identificado.';
    }
}
