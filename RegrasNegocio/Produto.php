<?php

namespace RegrasNegocio;

use App\Http\Requests\Produto\JurosProdutoRequest as JurosRequest;
use App\Models\Produto as Model;
use Symfony\Component\HttpFoundation\JsonResponse;

class Produto
{
    public function cacularJuros(JurosRequest $request): JsonResponse
    {
        $produto = Model::find($request->input('produto_id'));
        $taxa = $produto->categoria->getAttribute('taxa');
        $valor = $produto->valor / $request->input('numero_parcelas');

        if ($request->input('tipo_juros') == 'Simples') {

            return $this->jurosSimples($valor, $taxa, $request->input('numero_parcelas'));
        } else if ($request->input('tipo_juros') == 'Composto') {

            return $this->jurosComposto($valor, $taxa, $request->input('numero_parcelas'));
        }
    }

    private function jurosSimples(float $valor, float $taxa, float $parcelas): JsonResponse
    {
        $mensalidades = [];

        $valor *= $taxa;
        for ($x = 1; $x <= $parcelas; $x++) {
            array_push($mensalidades, round($valor, 2, PHP_ROUND_HALF_UP));
        }

        return response()->json(['data' => ['mensalidades' => $mensalidades]]);
    }

    private function jurosComposto(float $valor, float $taxa, float $parcelas): JsonResponse
    {
        $mensalidades = [];

        for ($x = 1; $x <= $parcelas; $x++) {
            $valor *= $taxa;
            array_push($mensalidades, round($valor, 2, PHP_ROUND_HALF_UP));
        }

        return response()->json(['data' => ['mensalidades' => $mensalidades]]);
    }
}
