<?php

namespace Tests\Feature\Categoria;

use App\Models\Categoria;
use Tests\TestCase;

class BuscarTest extends TestCase
{
    private const ROTA = 'categoria.show';
    private const ID_INVALIDO = 0;

    public function testCategoriaInvalido()
    {
        $response = $this->getJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $categoria = Categoria::factory()->create();

        $response = $this->getJson(route(self::ROTA, $categoria->getKey()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'nome'
                ],
            ]);
    }
}
