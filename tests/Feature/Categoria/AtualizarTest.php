<?php

namespace Tests\Feature\Categoria;

use App\Models\Categoria;
use Tests\TestCase;

class AtualizarTest extends TestCase
{
    private const ROTA = 'categoria.update';
    private const ID_INVALIDO = 0;

    public function setUp(): void
    {
        parent::setUp();
        $this->categoria = Categoria::factory()->create();
    }

    public function testFalhaCategoriaInvalida()
    {
        $dados = Categoria::factory()->make();

        $response = $this->putJson(route(self::ROTA, self::ID_INVALIDO), $dados->toArray());
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $dados = Categoria::factory()->make([
            'nome' => str_pad('', 101, 'A'),
        ]);

        $response = $this->putJson(route(self::ROTA, $this->categoria->getKey()), $dados->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $dados = [
            'nome' => null,
            'taxa' => null,
        ];

        $response = $this->putJson(route(self::ROTA, $this->categoria->getKey()), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'taxa',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $dados = [
            'nome' => 123,
            'taxa' => 'String',
        ];

        $response = $this->putJson(route(self::ROTA, $this->categoria->getKey()), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'taxa',
                ],
            ]);
    }

    public function testFalhaTaxaNegativa()
    {
        $dados = [
            'nome' => 'Categoria Nova',
            'taxa' => -1,
        ];

        $response = $this->putJson(route(self::ROTA, $this->categoria->getKey()), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'taxa',
                ],
            ]);
    }

    public function testFalhaTaxaFormatoInvalido()
    {
        $dados = [
            'nome' => 'Categoria Nova',
            'taxa' => 1.4444,
        ];

        $response = $this->putJson(route(self::ROTA, $this->categoria->getKey()), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'taxa',
                ],
            ]);
    }

    public function testSucesso()
    {
        $categoria = Categoria::factory()->make();
        $response  = $this->putJson(route(self::ROTA, $this->categoria->getKey()), $categoria->toArray());

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'nome',
                    'taxa',
                ],
            ]);
    }
}
