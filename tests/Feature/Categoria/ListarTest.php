<?php

namespace Tests\Feature\Categoria;

use App\Models\Categoria;
use Tests\TestCase;

class ListarTest extends TestCase
{
    private const ROTA = 'categoria.index';

    public function setUp(): void
    {
        parent::setUp();
        Categoria::factory()->count(3)->create();
    }

    public function testSucesso()
    {
        $response = $this->getJson(route(self::ROTA));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'nome',
                    ]
                ]
            ]);
    }
}
