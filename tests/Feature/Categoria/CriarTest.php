<?php

namespace Tests\Feature\Categoria;

use App\Models\Categoria;
use Tests\TestCase;

class CriarTest extends TestCase
{
    private const ROTA = 'categoria.store';

    public function testFalhaValoresGrandes()
    {
        $dados = Categoria::factory()->make([
            'nome' => str_pad('', 101, 'A'),
        ]);

        $response = $this->postJson(route(self::ROTA), $dados->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $dados = [
            'nome' => null,
            'taxa' => null,
        ];

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'taxa',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $dados = [
            'nome' => 123,
            'taxa' => 'String',
        ];

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'taxa',
                ],
            ]);
    }

    public function testFalhaTaxaNegativa()
    {
        $dados = [
            'nome' => 'Categoria Nova',
            'taxa' => -1,
        ];

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'taxa',
                ],
            ]);
    }

    public function testFalhaTaxaFormatoInvalido()
    {
        $dados = [
            'nome' => 'Categoria Nova',
            'taxa' => 1.4444,
        ];

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'taxa',
                ],
            ]);
    }

    public function testSucesso()
    {
        $categoria = Categoria::factory()->make();
        $response  = $this->postJson(route(self::ROTA), $categoria->toArray());

        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'nome',
                    'taxa',
                ],
            ]);
    }
}
