<?php

namespace Tests\Feature\Categoria;

use App\Models\Categoria;
use App\Models\Produto;
use Tests\TestCase;

class ApagarTest extends TestCase
{
    private const ROTA = 'categoria.destroy';
    private const ID_INVALIDO = 0;

    public function testCategoriaInvalido()
    {
        $response = $this->deleteJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testProdutoVinculado()
    {
        $categoriaId = Categoria::factory()->create()->getKey();
        Produto::factory()->create(['categoria_id' => $categoriaId]);

        $response = $this->deleteJson(route(self::ROTA, $categoriaId));
        $response->assertStatus(409)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $categoria = Categoria::factory()->create();

        $response = $this->deleteJson(route(self::ROTA, $categoria->getKey()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'message'
            ]);
    }
}
