<?php

namespace Tests\Feature\Produto;

use App\Models\Produto;
use Tests\TestCase;

class CriarTest extends TestCase
{
    private const ROTA = 'produto.store';

    public function testFalhaValoresGrandes()
    {
        $valorGrande = str_pad('', 101, 'A');
        $dados = Produto::factory()->make([
            'nome' => $valorGrande,
            'descricao' => $valorGrande,
        ]);

        $response = $this->postJson(route(self::ROTA), $dados->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'descricao',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $dados = [
            'nome'         => null,
            'descricao'    => null,
            'valor'        => null,
            'categoria_id' => null,
        ];

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'descricao',
                    'valor',
                    'categoria_id',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $dados = [
            'nome'         => 123,
            'descricao'    => 123,
            'valor'        => 'string',
            'categoria_id' => 'string',
        ];

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'descricao',
                    'valor',
                    'categoria_id',
                ],
            ]);
    }

    public function testSucesso()
    {
        $dados    = Produto::factory()->make();
        $response = $this->postJson(route(self::ROTA), $dados->toArray());

        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'nome',
                ],
            ]);
    }
}
