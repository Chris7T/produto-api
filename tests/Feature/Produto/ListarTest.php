<?php

namespace Tests\Feature\Produto;

use App\Models\Produto;
use Tests\TestCase;

class ListarTest extends TestCase
{
    private const ROTA = 'produto.index';

    public function setUp(): void
    {
        parent::setUp();
        Produto::factory()->count(3)->create();
    }

    public function testSucesso()
    {
        $response = $this->getJson(route(self::ROTA));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'nome',
                        'descricao',
                        'valor',
                        'categoria_id',
                        'categoria_nome'
                    ]
                ]
            ]);
    }
}
