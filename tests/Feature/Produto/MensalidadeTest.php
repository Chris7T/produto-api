<?php

namespace Tests\Feature\Produto;

use App\Models\Produto;
use Tests\TestCase;

class MensalidadeTest extends TestCase
{
    private const ROTA = 'produto.mensalidade';
    private const ID_INVALIDO = 0;

    public function testFalhaValoresGrandes()
    {
        $dados = Produto::factory()->make([
            'numero_parcelas' => str_pad('', 101, 'A'),
            'tipo_juros' => 99999,
        ]);

        $response = $this->postJson(route(self::ROTA), $dados->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'numero_parcelas',
                    'tipo_juros',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $dados = [
            'produto_id'      => null,
            'numero_parcelas' => null,
            'tipo_juros'      => null
        ];

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'produto_id',
                    'numero_parcelas',
                    'tipo_juros',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $dados = [
            'produto_id'      => 'string',
            'numero_parcelas' => 'string',
            'tipo_juros'      => 123,
        ];

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'produto_id',
                    'numero_parcelas',
                    'tipo_juros',
                ],
            ]);
    }

    public function testFalhaProdutoInvalido()
    {
        $dados = [
            'produto_id'      => self::ID_INVALIDO,
            'numero_parcelas' => 5,
            'tipo_juros'      => 'Composto',
        ];

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'produto_id',
                ],
            ]);
    }

    public function testFalhaTipoJurosInvalido()
    {
        $dados = [
            'produto_id'      => Produto::factory()->create()->getKey(),
            'numero_parcelas' => 5,
            'tipo_juros'      => 'String',
        ];

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'tipo_juros',
                ],
            ]);
    }

    public function testSucesso()
    {
        $dados = [
            'produto_id'      => Produto::factory()->create()->getKey(),
            'numero_parcelas' => 5,
            'tipo_juros'      => 'Composto',
        ];
        $response = $this->postJson(route(self::ROTA), $dados);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'mensalidades',
                ],
            ]);
    }
}
