<?php

namespace Tests\Feature\Produto;

use App\Models\Produto;
use Tests\TestCase;

class AtualizarTest extends TestCase
{
    private const ROTA = 'produto.update';
    private const ID_INVALIDO = 0;

    public function setUp(): void
    {
        parent::setUp();
        $this->produto = Produto::factory()->create();
    }

    public function testProdutoInvalido()
    {
        $valorGrande = str_pad('', 101, 'A');
        $dados = Produto::factory()->make([
            'nome' => $valorGrande,
            'descricao' => $valorGrande,
        ]);

        $response = $this->putJson(route(self::ROTA, self::ID_INVALIDO), $dados->toArray());
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $valorGrande = str_pad('', 101, 'A');
        $dados = Produto::factory()->make([
            'nome' => $valorGrande,
            'descricao' => $valorGrande,
        ]);

        $response = $this->putJson(route(self::ROTA, $this->produto->getKey()), $dados->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'descricao',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $dados = [
            'nome'         => null,
            'descricao'    => null,
            'valor'        => null,
            'categoria_id' => null,
        ];

        $response = $this->putJson(route(self::ROTA, $this->produto->getKey()), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'descricao',
                    'valor',
                    'categoria_id',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $dados = [
            'nome'         => 123,
            'descricao'    => 123,
            'valor'        => 'string',
            'categoria_id' => 'string',
        ];

        $response = $this->putJson(route(self::ROTA, $this->produto->getKey()), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'descricao',
                    'valor',
                    'categoria_id',
                ],
            ]);
    }

    public function testSucesso()
    {
        $dados    = Produto::factory()->make();
        $response = $this->putJson(route(self::ROTA, $this->produto->getKey()), $dados->toArray());

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'nome',
                    'descricao',
                    'valor',
                    'categoria_id',
                    'categoria_nome'
                ],
            ]);
    }
}
