<?php

namespace Tests\Feature\Produto;

use App\Models\Produto;
use Tests\TestCase;

class ApagarTest extends TestCase
{
    private const ROTA = 'produto.destroy';
    private const ID_INVALIDO = 0;

    public function testProdutoInvalido()
    {
        $response = $this->deleteJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $Produto = Produto::factory()->create();

        $response = $this->deleteJson(route(self::ROTA, $Produto->getKey()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'message'
            ]);
    }
}
