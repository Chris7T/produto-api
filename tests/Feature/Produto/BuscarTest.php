<?php

namespace Tests\Feature\Produto;

use App\Models\Produto;
use Tests\TestCase;

class BuscarTest extends TestCase
{
    private const ROTA = 'produto.show';
    private const ID_INVALIDO = 0;

    public function testProdutoInvalido()
    {
        $response = $this->getJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $Produto = Produto::factory()->create();

        $response = $this->getJson(route(self::ROTA, $Produto->getKey()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'nome',
                    'descricao',
                    'valor',
                    'categoria_id',
                    'categoria_nome'
                ],
            ]);
    }
}
