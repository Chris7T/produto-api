<?php

use Illuminate\Support\Facades\Route;

Route::apiResource('categoria', 'CategoriaController')->parameter('categoria', 'categoria');
