<?php

use Illuminate\Support\Facades\Route;

Route::prefix('produto')->name('produto.')->group(function () {
    Route::apiResource('/', 'ProdutoController')->parameters(['' => 'produto']);
    Route::post('/mensalidade', 'ProdutoController@mensalidades')->name('mensalidade');
});
